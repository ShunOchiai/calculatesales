package jp.alhinc.ochiai_shun.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static boolean fileExport(String path, String fileName, HashMap<String, Long>saleMap, HashMap<String, String>nameMap){
		BufferedWriter bw = null;
		try{
			File outFile = new File(path , fileName);
			outFile.createNewFile();
			FileWriter fw = new FileWriter(outFile);
			bw = new BufferedWriter(fw);
			for(String mapKey : saleMap.keySet() ) {
				bw.write(mapKey +","+ nameMap.get(mapKey) +","+ saleMap.get(mapKey) + System.lineSeparator());
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(bw != null) {
				try {
				bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	public static boolean fileImport(String path, String fileName, HashMap<String, Long>saleMap , HashMap<String, String>nameMap, String fileChecker, String definitionName){
		FileReader fr = null;
		BufferedReader br = null;
		try{
			File files = new File(path, fileName);
			if(!files.exists()) {
				System.out.println(definitionName + "定義ファイルが存在しません");
				return false;
			}
			fr = new FileReader(files);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
				String branchesFile[] = line.split( "," , 0);
				if(branchesFile.length  != 2) {
					System.out.println(definitionName + "定義ファイルのフォーマットが不正です");
					return false;
				}
				if(!branchesFile[0].matches(fileChecker)) {
					System.out.println(definitionName + "定義ファイルのフォーマットが不正です");
					return false;
				}
				nameMap.put(branchesFile[0], branchesFile[1]);
				saleMap.put(branchesFile[0], (long)0);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
		}finally{
			if((br != null) || (fr != null)){
				try{
					br.close();
					fr.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	return true;
	}

	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		HashMap<String, String> branchesMap = new HashMap<String, String >();
		HashMap<String, Long> salesMap = new HashMap<String, Long >();
		/*
		try{
			File files = new File(args[0], "branch.lst");
			if(!files.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(files);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
				String branchesFile[] = line.split( "," , 0);
				if(branchesFile.length  != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				if(!branchesFile[0].matches("[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				branchesMap.put(branchesFile[0], branchesFile[1]);
				salesMap.put(branchesFile[0], (long)0);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
		}finally{
			if(br != null){
				try{
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}*/
		if(!fileImport(args[0], "branch.lst", salesMap, branchesMap, "[0-9]{3}", "支店")) {
			return;
		}

		ArrayList <File> rcdFileList = new ArrayList<File>();
		File mf = new File(args[0]);
		File[] manyFiles = mf.listFiles();
		for(File analyzeFile : manyFiles) {
			String strAnalyzeFile = analyzeFile.getName();
			if(strAnalyzeFile.matches("[0-9]{8}.rcd") && analyzeFile.isFile()){
				rcdFileList.add(analyzeFile);
			}
		}//連番チェック
		for(int i = 0 ; i < (rcdFileList.size() - 1) ; i++) {
			int j = Integer.parseInt(rcdFileList.get(i).getName().substring(0, 8));
			if(Integer.parseInt(rcdFileList.get(i + 1).getName().substring(0, 8)) - j != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;
		FileReader fr = null;
		long sumSell;
		try{
			for(int i = 0; i < rcdFileList.size(); i++) {
				ArrayList<String> rcdCulcList = new ArrayList<String>();
				String rcdReadLine ;
				File rcdFile = rcdFileList.get(i);
				fr = new FileReader(rcdFile);
				br = new BufferedReader(fr);
				while((rcdReadLine = br.readLine()) != null){
					rcdCulcList.add(rcdReadLine);
				}
				if(rcdCulcList.size() != 2) {
					System.out.println(rcdFile.getName() + "のフォーマットが不正です");
					return;
				}
				if(!salesMap.containsKey(rcdCulcList.get(0))) {
					System.out.println(rcdFile.getName() + "の支店コードが不正です");
					return;
				}
				if(!rcdCulcList.get(1).matches("[0-9]+")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				sumSell = salesMap.get(rcdCulcList.get(0)) + Long.parseLong(rcdCulcList.get(1));
				if(sumSell > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				salesMap.put(rcdCulcList.get(0), sumSell);
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally{
			if((br != null) || (fr != null)){
				try {
					br.close();
					fr.close();
				}catch (IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		/*
		BufferedWriter bw = null;
		try{
			File outFile = new File(args[0], "branch.out");
			outFile.createNewFile();
			FileWriter fw = new FileWriter(outFile);
			bw = new BufferedWriter(fw);
			for( String sMapKey : salesMap.keySet()) {
				bw.write(sMapKey +","+ branchesMap.get(sMapKey) +","+ salesMap.get(sMapKey) + System.lineSeparator());
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally{
			if(bw != null) {
				try {
				bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}*/
		if(!fileExport(args[0], "branch.out", salesMap, branchesMap)) {
			return;
		}
	}
}